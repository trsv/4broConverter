#ifndef NETWORKWORKER_H
#define NETWORKWORKER_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkCookieJar>
#include <QtNetwork/QNetworkCookie>
#include <QEventLoop>
#include <memory>
#include "struct.h"
#include "def.h"

class NetworkWorker : public QObject
{
    Q_OBJECT
public:
    explicit NetworkWorker(QObject *parent = 0);

signals:
    void SignalError(const QString &);
    void SignalProcesing(const QString &);
public slots:
    bool FindSup(Template &t);
    void DeleteSupData(const QString & id);
    void SetPathFile(const QString & path)
    {
        m_userInfo.m_pathFile = path;
        m_userInfo.m_nameFile = path.mid(path.lastIndexOf("/") + 1,  path.length());
    }

    void SetSuplier()
    {
        for(auto element = ALL_INFO_SUPPLIER.cbegin(); element != ALL_INFO_SUPPLIER.cend(); ++element)
        {
            m_listSupplier.insert(element.key(), element.value());
        }
    }

    void setSuplier(const QString & name) {
        m_suplier = name;
    }

    void setAuth(const QString & login, const QString & password) {
        m_userInfo.m_login = login;
        m_userInfo.m_pass = password;

        GetCookies();
        LoginAdminOnWeb();
    }

    void GetSupplierInfo(QList<Supplier> &list, int id);
    void PostImport(const Supplier &supplier);
    // записать в m_listSupplier список поставщиков
    void setListSupplier();

private slots:
    void GetCookies();
    void LoginAdminOnWeb();
    void StartImport();

    void LoadInfoFromFile();
    void Load(const QString & url, const int & row = 0);
    QString GetValue(const QString &,  QString &);

public:
    // поставщики: имя - id
    QMap <QString, int> m_listSupplier;

private:
    std::unique_ptr<QNetworkAccessManager> m_manager;
    QList<QNetworkCookie> m_cookies;
    QNetworkCookieJar * m_jar;
    QNetworkReply *m_reply;
    QEventLoop m_loop;
    UserInfo m_userInfo;
    int indexSup;

    QString m_suplier;
};

#endif // NETWORKWORKER_H
