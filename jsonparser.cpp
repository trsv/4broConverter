#include "jsonparser.h"

JSONParser::JSONParser()
{

}

JSONParser::~JSONParser()
{

}

void JSONParser::GetSupplier(QString & strBuff, QList<Supplier> & supp)
{
    supp.clear();

    QJsonDocument jsonDoc = QJsonDocument::fromJson(strBuff.toUtf8());
    QJsonArray jsonArray = jsonDoc.object()["samples"].toArray();
    Supplier sup;
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        sup.m_nameSup = obj["name"].toString();
        sup.m_sample_id = obj["id"].toString();
        sup.m_supplierId = obj["supplier_id"].toString();
        sup.m_attrubutes = obj["value"].toObject()["attributes"].toString();
        sup.m_brand = obj["value"].toObject()["brand"].toString();
        sup.m_category = obj["value"].toObject()["category"].toString();
        sup.m_currencyId = obj["value"].toObject()["currency_id"].toString();
        sup.m_default_category_id = obj["value"].toObject()["default_category_id"].toString();
        sup.m_default_excerpt = obj["value"].toObject()["default_excerpt"].toString();
        sup.m_default_regular = obj["value"].toObject()["default_regular"].toString();
        sup.m_default_term = obj["value"].toObject()["default_term"].toString();
        sup.m_default_term_unit = obj["value"].toObject()["default_term_unit"].toString();
        sup.m_delivery_price = obj["value"].toObject()["delivery_price"].toString();
        sup.m_description = obj["value"].toObject()["description"].toString();
        sup.m_excerpt = obj["value"].toObject()["excerpt"].toString();
        sup.m_image = obj["value"].toObject()["image"].toString();
        sup.m_name = obj["value"].toObject()["name"].toString();
        sup.m_quantity = obj["value"].toObject()["quantity"].toString();
        sup.m_saleprice = obj["value"].toObject()["saleprice"].toString();
        sup.m_sku = obj["value"].toObject()["sku"].toString();
        sup.m_term = obj["value"].toObject()["term"].toString();

        supp.push_back(sup);
    }


}

QString JSONParser::GetValue(const QString & key, QString & strBuff)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(strBuff.toUtf8());
    QJsonObject obj = jsonDoc.object();
    return obj[key].toString();
}
