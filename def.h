#ifndef DEF_H
#define DEF_H
#include <QUrl>
#include <QString>
#include <QMap>

const QString PATH_TO_CONVERTED = "converted";

const QString GET_FIRST_REQUEST = "https://4bro.pro/autoxadmin";
const QString GET_IMPORT_PAGE = "https://4bro.pro/autoxadmin/import";
const QString GET_SUPPLIER_INFO = "https://4bro.pro/autoxadmin/supplier/get_sample_app";
const QString GET_DELETE = "https://4bro.pro/autoxadmin/supplier/delete_products/";
const QString GET_HREF = "href='";
const QMap<QString, int> ALL_INFO_SUPPLIER = {
    {"Интеркарс", 1},
    {"МТехно", 2 },
    {"ASG", 10 },
    {"АВД", 11},
    {"Элит", 9 },
    {"Автотехникс", 5},
    {"Владислав", 6 },
    {"Омега", 7 },
    {"Юник Трейд", 8 },
    {"Элит оригинал", 12 },
    {"ТаймПартс", 13 },
    {"ЮнитПартс", 14 },
    {"Эксклюзив",15},
    {"Бусмаркет", 17},
    {"SCAR", 18},
    {"Масло", 20 },
    {"Мастер",25},
    {"БусЛТ", 24},
    {"Автопро", 22}
};
#endif // DEF_H
