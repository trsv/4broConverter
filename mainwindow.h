#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "struct.h"
#include "qjsonmodel.h"`

#include <memory>

#include <QMainWindow>
#include <QStandardItemModel>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void SlotError(const QString &);
    void LoadSlot(const QString &);

    void setButtonImportEnable(bool value);

signals:
    void dataPrepared(QList <Template> & list);
    void updateListTemplate(const QString & supplier);
private slots:
    void on_buttonConvert_clicked();
    void on_buttonSaveSettings_clicked();

    void showMessageError(const QString & message);
    void clearDir(const QString & path);

public:
    QString m_login;
    QString m_password;

private:
    Ui::MainWindow *ui;
    std::unique_ptr <QJsonModel> m_model;
    QLabel * m_lableSt;
};

#endif // MAINWINDOW_H
