#ifndef PARSER_H
#define PARSER_H

#include <QtCore>

class Parser
{
public:
    Parser();
public:
    QString GetValue(const QString&, QString&);
};

#endif // PARSER_H
