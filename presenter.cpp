#include "presenter.h"
#include "def.h"

#include <QtConcurrent/QtConcurrent>

Presenter::Presenter(QObject * parent) : QObject(parent)
{
    m_mainWindow.reset(new MainWindow());
    m_Worker.reset(new NetworkWorker());

    m_mainWindow->show();

    if (!m_mainWindow->m_login.isEmpty() && !m_mainWindow->m_password.isEmpty()) {
        m_Worker->setAuth(m_mainWindow->m_login, m_mainWindow->m_password);
        m_Worker->SetSuplier();
        //m_Worker->setListSupplier();
    } else {
        SlotError("Логин/пароль не указаны в настройках");
        m_mainWindow->setButtonImportEnable(false);
    }

    QObject::connect(m_mainWindow.get(), &MainWindow::dataPrepared, this, &Presenter::dataProcess);

    if (m_Worker->m_listSupplier.isEmpty()) {
        SlotProcesing("Не удалось получить список поставщиков");
        m_mainWindow->setButtonImportEnable(false);
    }

}

void Presenter::runDataProcess(QList<Template> &list)
{
    m_threadDataProcess = QtConcurrent::run(this, &Presenter::dataProcess, list);
    m_watcher.setFuture(m_threadDataProcess);
}

void Presenter::dataProcess(QList<Template> &list)
{
    QObject::connect(m_Worker.get(), &NetworkWorker::SignalError, this, &Presenter::SlotError, Qt::DirectConnection);
    QObject::connect(m_Worker.get(), &NetworkWorker::SignalProcesing, this, &Presenter::SlotProcesing, Qt::DirectConnection);

    if (m_mainWindow->m_login.isEmpty() || m_mainWindow->m_password.isEmpty()) {
        SlotError("Логин/пароль не указаны в настройках");
        return;
    }

    for (Template & t : list) {
        if (!m_Worker->FindSup(t)) {
            SlotError("Прайс '" + t.file + "' (поставщик: " + t.supplier + ")" + "\nне найден в списке поставщиков на сайте");
            return;
        }
    }

    // загружаем весь список на сайт
    for (Template & t : list) {

        qDebug() << t.file << t.supplier << t.templates << t.allTemplates;

        m_Worker->SetPathFile(t.file);
        m_Worker->setSuplier(t.supplier);

        QList <Supplier> listTemplateOnSite;
        m_Worker->GetSupplierInfo(listTemplateOnSite, t.idSupplier);

        if (t.clearData) m_Worker->DeleteSupData(listTemplateOnSite.at(0).m_supplierId);

        for (Supplier & supplier : listTemplateOnSite) {
            if (t.allTemplates || t.templates.contains(supplier.m_nameSup)) {
                m_Worker->PostImport(supplier);
                qDebug() << "шаблон поставщика:" << supplier.m_nameSup;
            }
        }

    }

    SlotProcesing("Все файлы импортированы");
}

void Presenter::SlotError(const QString & s)
{
    m_mainWindow->SlotError(s);
}

void Presenter::SlotProcesing(const QString & txt)
{
    m_mainWindow->LoadSlot(txt);
}
