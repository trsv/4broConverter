#ifndef PRESENTER_H
#define PRESENTER_H

#include "mainwindow.h"

#include <QDir>
#include <QObject>
#include <QFuture>
#include <QFutureWatcher>

#include <memory>

#include "networkworker.h"
class Presenter : public QObject
{
    Q_OBJECT

public:
    explicit Presenter(QObject * parent = 0);

private slots:
    void runDataProcess(QList <Template> & list);
    void SlotError(const QString &);
    void SlotProcesing(const QString &);
private:
    void dataProcess(QList <Template> & list);

    void clearDir(const QString & path);

private:
    std::unique_ptr <MainWindow> m_mainWindow;
    std::unique_ptr <NetworkWorker> m_Worker;
    QFuture<void> m_threadDataProcess;
    QFutureWatcher<void> m_watcher;
};

#endif // PRESENTER_H
