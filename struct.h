﻿#ifndef STRUCT_H
#define STRUCT_H

#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

/**
 * Структура для значений шаблона
 * */
struct Supplier
{
    QString m_nameSup;
    QString m_default_category_id;
    QString m_currencyId;
    QString m_supplierId;
    QString m_default_excerpt;
    QString m_default_term;
    QString m_default_term_unit;
    QString m_default_regular;
    QString m_sku;
    QString m_brand;
    QString m_name;
    QString m_description;
    QString m_excerpt;
    QString m_delivery_price;
    QString m_saleprice;
    QString m_quantity;
    QString m_term;
    QString m_category;
    QString m_image;
    QString m_attrubutes;
    QString m_sample_id;
};

/*
 * Структура для инфы про пользователя и для пути к файлу и его имени
 * */
struct UserInfo
{
    QString m_pass;
    QString m_login;
    QString m_nextLocation;
    QString m_pathFile;
    QString m_nameFile;
    QString m_nameModul;
    QList<Supplier> m_listSupp;
};

struct Template {

    QString file;

    int idSupplier;
    QString supplier;

    bool allTemplates;
    QList <QString> templates;

    bool clearData;

};

#endif // STRUCT_H
