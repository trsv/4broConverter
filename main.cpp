#include <QApplication>

#include "presenter.h"
#include <memory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::unique_ptr<Presenter>  presenter(new Presenter());

    return a.exec();
}
