#include "networkworker.h"
#include "def.h"
#include "jsonparser.h"

#include <QFile>
#include <QRegularExpression>

NetworkWorker::NetworkWorker(QObject *parent) : QObject(parent), indexSup(0)
{
    m_manager.reset(new QNetworkAccessManager);
    m_jar = new QNetworkCookieJar;
    m_manager->setCookieJar(m_jar);
}

void NetworkWorker::DeleteSupData(const QString &id)
{
    QString ur1 = GET_DELETE + id;
    QNetworkRequest mRequest;
    QUrl url(ur1);

    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);
    //mRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    m_reply = m_manager->get(mRequest);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            qDebug() << "Success!!";
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

void NetworkWorker::GetCookies()
{
    QNetworkRequest mRequest;
    mRequest.setUrl(QUrl(GET_FIRST_REQUEST));
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");

    m_reply = m_manager->get(mRequest);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            m_cookies = qvariant_cast <QList <QNetworkCookie> > (m_reply->header(QNetworkRequest::SetCookieHeader));
            m_userInfo.m_nextLocation = m_reply->header(QNetworkRequest::LocationHeader).toString();
            qDebug() << m_cookies[0].name() << "      " << m_cookies[0].value()  << "\n";
            qDebug() << "Location    = " <<  m_userInfo.m_nextLocation << "\n";
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

void NetworkWorker::LoginAdminOnWeb()
{
    QNetworkRequest mRequest;
    QUrl url(m_userInfo.m_nextLocation);
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);
    QByteArray arr;
    arr.append("email=" + m_userInfo.m_login);
    arr.append("&password=" + m_userInfo.m_pass);

    m_reply = m_manager->post(mRequest, arr);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            m_userInfo.m_nextLocation = m_reply->header(QNetworkRequest::LocationHeader).toString();
            qDebug() <<"Location    = " <<   m_userInfo.m_nextLocation << "\n";
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

void NetworkWorker::StartImport()
{
    QNetworkRequest mRequest;
    QUrl url(GET_IMPORT_PAGE);
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);

    m_reply = m_manager->get(mRequest);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            m_userInfo.m_nextLocation = m_reply->header(QNetworkRequest::LocationHeader).toString();
            qDebug() <<"Location    = " <<   m_userInfo.m_nextLocation << "\n";
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

void NetworkWorker::GetSupplierInfo(QList <Supplier> & list, int id)
{
    QNetworkRequest mRequest;
    QUrl url(GET_SUPPLIER_INFO);
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("X-Requested-With", "XMLHttpRequest");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    mRequest.setRawHeader("Accept", "*/*");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);

    QByteArray arr;

    arr.append("supplier_id=");
    arr.append(QString::number(id));

    m_reply = m_manager->post(mRequest, arr);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    JSONParser parserJs;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            parserJs.GetSupplier(result2, m_userInfo.m_listSupp);

            list = m_userInfo.m_listSupp;

            //if(!m_userInfo.m_listSupp.isEmpty())
            //PostImport();
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}
// Метод для загрузки настроек в импорт
// https://4bro.pro/autoxadmin/import/add?settings=1&supplier_id=2
// Следующий запрос после редиректа
void NetworkWorker::PostImport(const Supplier & supplier)
{
    QNetworkRequest mRequest;
    QUrl url(GET_IMPORT_PAGE);


    QString boundary("------WebKitFormBoundary");
    QByteArray data;
    // №1
    data +=   boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"filename\"; filename=\"" + m_userInfo.m_nameFile  + "\"\r\n";
    //    if(m_userInfo.m_nameFile.indexOf(".xls") != -1)
    //    {
    data += "Content-Type: application/vnd.ms-excel\r\n\r\n";
    //    }
    QFile file(m_userInfo.m_pathFile);

    qDebug() << "path to file:" << m_userInfo.m_pathFile;

    if(file.open(QIODevice::ReadOnly))
    {
        data.append(file.readAll());
        file.close();
    }
    else
    {
        qDebug() << "ERROR OPEN FILE, READ LOAD FILE!!!\n";
        qDebug() << "Path" << m_userInfo.m_pathFile;
    }

    // №2
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"supplier_id\"\r\n\r\n";
    data += supplier.m_supplierId + "\r\n";
    // №3
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample_id\"\r\n\r\n";
    data += supplier.m_sample_id + "\r\n";
    // №4
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample_name\"\r\n\r\n";
    data += "\r\n";
    // №5
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[currency_id]\"\r\n\r\n";
    data += supplier.m_currencyId +"\r\n";
    // №6
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[default_category_id]\"\r\n\r\n";
    data += m_userInfo.m_listSupp[indexSup].m_default_category_id + "\r\n";
    // №7
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[default_excerpt]\"\r\n\r\n";
    data += supplier.m_default_excerpt + "\r\n";
    // №8
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[default_term]\"\r\n\r\n";
    data += supplier.m_default_term + "\r\n";
    // №9
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[default_term_unit]\"\r\n\r\n";
    data += supplier.m_default_term_unit + "\r\n";
    // №10
    data += boundary + "\r\n";
    data += " Content-Disposition: form-data; name=\"sample[default_regular]\"\r\n\r\n";
    data += supplier.m_default_regular +"\r\n";
    // №11
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[sku]\"\r\n\r\n";
    data += supplier.m_sku +"\r\n";
    // №12
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[brand]\"\r\n\r\n";
    data += supplier.m_brand + "\r\n";
    // №13
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[name]\"\r\n\r\n";
    data += supplier.m_name + "\r\n";
    // №14
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[description]\"\r\n\r\n";
    data += supplier.m_description + "\r\n";
    // №15
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[excerpt]\"\r\n\r\n";
    data += supplier.m_excerpt +  "\r\n";
    // №16
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[delivery_price]\"\r\n\r\n";
    data += supplier.m_delivery_price + "on\r\n";
    // №17
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[saleprice]\"\r\n\r\n";
    data += supplier.m_saleprice + "\r\n";
    // №18
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[quantity]\"\r\n\r\n";
    data += supplier.m_quantity +"\r\n";
    // №19
    data +=  boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[term]\"\r\n\r\n";
    data += supplier.m_term +"\r\n";
    // №20
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[category]\"\r\n\r\n";
    data += supplier.m_category +"\r\n";
    // №21
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[image]\"\r\n\r\n";
    data += supplier.m_image +"\r\n";
    // №22
    data += boundary + "\r\n";
    data += "Content-Disposition: form-data; name=\"sample[attributes]\"\r\n\r\n";
    data +=  supplier.m_attrubutes +"\r\n";
    data += "------WebKitFormBoundary--";
    QString strLength;
    strLength.append(data.length());
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("Content-Length", strLength.toStdString().c_str());
    mRequest.setRawHeader("X-Requested-With", "XMLHttpRequest");
    mRequest.setRawHeader("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");

    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);
    m_reply = m_manager->post(mRequest, data);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            if(result2.isEmpty())
                m_userInfo.m_nextLocation = m_reply->header(QNetworkRequest::LocationHeader).toString();
            else
                m_userInfo.m_nextLocation =  GetValue(GET_HREF, result2);

            if(!m_userInfo.m_nextLocation.isEmpty())
                LoadInfoFromFile();
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

bool NetworkWorker::FindSup(Template & t)
{
    auto supplier = m_listSupplier.find(t.supplier);

    if (supplier != m_listSupplier.end()) {
        t.idSupplier = supplier.value();
        return true;
    } else {
        t.idSupplier = -1;
        return false;
    }
}

void NetworkWorker::LoadInfoFromFile()
{
    QNetworkRequest mRequest;
    QUrl url(m_userInfo.m_nextLocation);
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);

    m_reply = m_manager->get(mRequest);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    JSONParser parserJs;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();

            if(result2.indexOf("Идет загрузка") != -1)
            {
                m_userInfo.m_nextLocation = GetValue(GET_HREF, result2);
                LoadInfoFromFile();
            }
            else if(result2.isEmpty() || result2.indexOf("Success!") != -1)
            {
                m_userInfo.m_nextLocation = m_reply->header(QNetworkRequest::LocationHeader).toString();
                Load("", 0); // Запуск загрузки на страницу, загрузка рекурсивная
            }
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

void NetworkWorker::Load(const QString & url1, const int & row)
{
    QString ur2;
    QTextStream ts(&ur2);
    // настройки сейчас стоят 1(Добавить и обновить)
    if(row == 0)
        ts << GET_IMPORT_PAGE << "/add?settings=" << 1 << "&supplier_id"  <<m_userInfo.m_listSupp[indexSup].m_supplierId;
    else
        ts << url1;
    QNetworkRequest mRequest;
    QUrl url(ur2);
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("Accept", "*/*");
    mRequest.setRawHeader("X-Requested-With", "XMLHttpRequest");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);

    m_reply = m_manager->get(mRequest);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    JSONParser parserJs;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            QString nextUrl = parserJs.GetValue("continue", result2).replace("\\", "");
            int nextRow =  QRegularExpression("\\/(\\d+)\\?").match(result2).captured(1).toInt();
            QString workSignal;
            QTextStream ts(&workSignal);
            ts << QString("Загрузка файла, ") <<  m_userInfo.m_nameFile << QString(", загружено строк ") << nextRow;

            emit SignalProcesing(workSignal);

            if(result2.indexOf("success") == -1)
                Load(nextUrl, nextRow);
            else
            {
                workSignal.clear();
                ts << QString("Загрузка файла ,") << m_userInfo.m_nameFile << QString(" успешно завершена.");
                emit SignalProcesing(workSignal);
            }
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}

QString NetworkWorker::GetValue(const QString& key, QString& strBuff)
{
    int indexStart = strBuff.indexOf(key) + key.length();
    int indexEnd = 0;
    QString strResult;
    if(indexStart - key.length() == -1)
        return QString::null;

    if(key == GET_HREF)
    {
        indexEnd = strBuff.indexOf("'", indexStart);
        strResult = strBuff.mid(indexStart, indexEnd - indexStart);
    }
    return strResult;
}

void NetworkWorker::setListSupplier()
{
    // отправляем запрос для получения списка поставщиков
    // парсим json ответ
    // записываем в m_listSupplier имя поставщика и id

    QNetworkRequest mRequest;
    QUrl url(GET_IMPORT_PAGE);
    mRequest.setRawHeader("Host", "4bro.pro");
    mRequest.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
    mRequest.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    m_jar->setCookiesFromUrl(m_cookies, url);
    mRequest.setUrl(url);

    m_reply = m_manager->get(mRequest);

    QObject::connect(m_reply, &QNetworkReply::finished, &m_loop, &QEventLoop::quit);
    m_loop.exec();

    QString result2;
    try
    {
        if(m_reply->error() == QNetworkReply::NoError)
        {
            result2 = m_reply->readAll();
            result2 = result2.simplified();

            QRegularExpression regex("<select.*?id=\"supplier\".*?<\\/select>");
            QString block = regex.match(result2).captured(0);

            regex.setPattern("<option value=\"(\\d+)\">(.*?)<\\/option>");

            QRegularExpressionMatchIterator i = regex.globalMatch(block);

            qDebug() << "Список поставщиков (id, название):";

            m_listSupplier.clear();
            while (i.hasNext()) {
                QRegularExpressionMatch match = i.next();

                QString name = match.captured(2);
                int id = match.captured(1).toInt();

                qDebug() << id << name;

                m_listSupplier.insert(name, id);
            }
        }
        else
        {
            const std::string str = m_reply->errorString().toStdString();
            throw std::runtime_error(str.c_str());
        }
    }
    catch(const std::exception & excep)
    {
        qDebug() << excep.what();
    }
}
