#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qjsonvalue.h>
#include "struct.h"

class JSONParser
{
public:
    JSONParser();
    virtual ~JSONParser();
public:
    void GetSupplier(QString &, QList<Supplier> &);
    QString GetValue(const QString &, QString & strBuff);

private:
    QJsonDocument m_jdoc;
    QJsonObject m_jobj;
    QJsonArray m_jarr;
};

#endif // JSONPARSER_H
