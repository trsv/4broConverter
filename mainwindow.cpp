#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "def.h"

#include <QFileDialog>
#include <QDirIterator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QDebug>
#include <QMessageBox>
#include <QSettings>
#include <QMap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_model.reset(new QJsonModel());
    ui->viewFileList->setModel(m_model.get());

    m_model->load("config.json");

    // запись в статус бар связка
    m_lableSt = new QLabel(this);
    ui->statusBar->addWidget(m_lableSt);

    QSettings api("api.ini", QSettings::IniFormat);

    bool clear = api.value("Site/clear", true).toBool();
    m_login = api.value("Site/login", "").toString();
    m_password = api.value("Site/password", "").toString();

    ui->lineLogin->setText(m_login);
    ui->linePassword->setText(m_password);

    ui->checkClearData->setChecked(clear);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonConvert_clicked()
{
    if (m_model->rowCount() == 0) {
        showMessageError("Список пуст. Добавьте файл(ы)");
        return;
    }

    QList <Template> list;
    for (int row = 0; row < m_model->rowCount(); row++) {
        QModelIndex index = m_model->index(row, 0);

        QString file = index.child(0, 1).data().toString();

        if (!QFileInfo(file).exists()) continue;

        QString supplier = index.child(1, 1).data().toString();

        QList <QString> templates;

        bool all = false;
        int count = index.child(2, 0).model()->columnCount();
        for (int i = 0; i < count; i++) {

            QString name = index.child(2, 0).child(i, 1).data().toString();
            if (name == "all") {
                all = true;
                break;
            }

            templates << name;
        }

        Template t;
        t.file = file;
        t.supplier = supplier;
        t.templates = templates;
        t.allTemplates = all;
        t.clearData = ui->checkClearData->isCheckable();

        list.append(t);
    }

    if (list.isEmpty()) {
        showMessageError("Все файлы из списка отсутствуют");
        return;
    }

    emit dataPrepared(list);
}

void MainWindow::showMessageError(const QString &message)
{
    QMessageBox::warning(this, "Ошибка", message, QMessageBox::Ok);
}

void MainWindow::clearDir(const QString &path)
{
    QDir dir(path);
    for (QString & file : dir.entryList()) {
        dir.remove(file);
    }
}

void MainWindow::SlotError(const QString & s)
{
    QMessageBox::warning(
                this,
                tr("Parser"),
                tr(s.toStdString().c_str()) );
}

void MainWindow::LoadSlot(const QString & txt)
{
    this->m_lableSt->setText(tr(txt.toStdString().c_str()));
}

void MainWindow::setButtonImportEnable(bool value)
{
    ui->buttonConvert->setEnabled(value);
}

void MainWindow::on_buttonSaveSettings_clicked()
{
    QSettings api("api.ini", QSettings::IniFormat);

    api.setValue("Site/login", ui->lineLogin->text());
    api.setValue("Site/password", ui->linePassword->text());
    api.setValue("Site/clear", ui->checkClearData->isChecked());

    m_login = ui->lineLogin->text();
    m_password = ui->linePassword->text();
}
